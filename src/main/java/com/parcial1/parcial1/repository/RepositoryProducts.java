package com.parcial1.parcial1.repository;

import com.parcial1.parcial1.model.Products;
import org.springframework.data.repository.CrudRepository;

public interface RepositoryProducts extends CrudRepository<Products, Integer> {

}
