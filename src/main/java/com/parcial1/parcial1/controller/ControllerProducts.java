package com.parcial1.parcial1.controller;

import com.parcial1.parcial1.model.Products;
import com.parcial1.parcial1.repository.RepositoryProducts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/product")
public class ControllerProducts {
    @Autowired
    RepositoryProducts repositoryProducts;

    @GetMapping("/all")
    public ResponseEntity<List<Products>> getAll() {
        List<Products> productsList = (List<Products>) repositoryProducts.findAll();

        List<Products> listaFiltrada = productsList.stream().filter(products -> {
            return products.getPrecio() > 10;
        }).collect(Collectors.toList());

        return ResponseEntity.ok(listaFiltrada);
    }

    @PostMapping("/create")
    public ResponseEntity<Products> createProduct(@RequestBody Products p) {
        return ResponseEntity.ok(repositoryProducts.save(p));
    }
}
